package programacionweb.controladores;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import programacionweb.modelos.Cifrado;
import programacionweb.modelos.Descifrado;


@WebServlet("/inicio")
public class ControladorInicio extends HttpServlet {

  protected void doGet(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException {
    
    
    var contextoServlet = solicitud.getServletContext();
    var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
    despachadorSolicitud.forward(solicitud, respuesta);
  }

        protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
            String resultado;
            String texto=request.getParameter("entrada-de-texto");
            String permu=request.getParameter("entrada-de-permutacion");
            String opc=request.getParameter("selector");
            String prueba=request.getParameter("cifrar");

            if(texto.length()>1 && texto.length()<1000){
               
              if(opc.toUpperCase().equals("CIFRADO")){
              resultado=Cifrado.metodoCifrar(texto, permu);
              }else{
                resultado=Descifrado.metodoDesCifrar(texto,permu);
                
                request.setAttribute("resultado", resultado);
              }
              request.setAttribute("resultado", resultado);
            }
          
            
            var contextoServlet = request.getServletContext();
            var despachadorSolicitud =
            contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
            despachadorSolicitud.forward(request, response);
            
          }

          private void processRequest(HttpServletRequest request, HttpServletResponse response) {
          }
        
        
}