package programacionweb.controladores;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import programacionweb.modelos.Cifrado;
import programacionweb.modelos.Descifrado;
import programacionweb.modelos.Atbash;


@WebServlet("/atbash")
public class ControladorAtbash extends HttpServlet {

  protected void doGet(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException {
    
    
    var contextoServlet = solicitud.getServletContext();
    var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaAtbash.jsp");
    despachadorSolicitud.forward(solicitud, respuesta);
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    String resultado;
    String alfabeto;
    String texto=request.getParameter("entrada-de-texto");
    String permu=request.getParameter("entrada-de-alfabeto");
    String opc=request.getParameter("selector");
    String prueba=request.getParameter("cifrar");
    alfabeto=request.getParameter("entrada-de-alfabeto");

    if(texto.length()>1 && texto.length()<1000){
       
      if(opc.toUpperCase().equals("CIFRADO")){
       resultado=Atbash.codificar(texto, permu);
      }else{
        resultado=Atbash.codificar(texto, permu);
        
        request.setAttribute("resultado", resultado);
        request.setAttribute("alfabeto", alfabeto);
      }
      request.setAttribute("resultado", resultado);
      request.setAttribute("alfabeto", alfabeto);
    }
  
    
    var contextoServlet = request.getServletContext();
    var despachadorSolicitud =
    contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaAtbash.jsp");
    despachadorSolicitud.forward(request, response);
    
  }

}