package programacionweb.modelos;
public class Atbash {

    public static String codificar(String texto, String alfabeto) {
        String resultado="";
        char[] separadorAux=alfabeto.toCharArray();
        char[] aux=alfabeto.toCharArray();
        char[] separadorCadena=aux;
        char[] separadorTexto=texto.toCharArray();
        int posicion;
        char tama;
        for (int i=0; i<separadorCadena.length/2; i++) {
            tama = separadorCadena[i];
            separadorCadena[i] = separadorCadena[separadorCadena.length-1-i];
            separadorCadena[separadorCadena.length-1-i] = tama;
        }
         for(int x = 0; x < separadorTexto.length ; x++) {
                for(int i = 0; i < separadorAux.length ; i++) {
                     posicion=texto.indexOf(separadorAux[i]);
                     while(posicion!=-1) {
                         separadorTexto[posicion]=separadorCadena[i];
                         posicion=texto.indexOf(separadorAux[i],posicion+1);
                     }
                }
            }
        for(int y = 0; y < separadorTexto.length ; y++) {
            resultado=resultado+String.valueOf(separadorTexto[y]);
        }
        return resultado;
    }
 
}
