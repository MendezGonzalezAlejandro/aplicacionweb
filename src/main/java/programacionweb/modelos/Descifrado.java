package programacionweb.modelos;
public class Descifrado {


public static String metodoDesCifrar(String texto, String permutacion) {
    String mensaje= "";
    if (verificaP(permutacion)) {
        mensaje = descifrado(texto, permutacion);;
    }else{
        mensaje ="Error, La permutacion no es valida !";
    }
    return mensaje; 
}

public static String Prueba(String texto, String permutacion) {
    String mensaje= "";
    return mensaje; 
}
public static boolean verificaP(String permutacion) {
    boolean estado = true;

    for (int i = 1; i <= permutacion.length(); i++) {
        if (permutacion.indexOf((char) (i + '0')) == -1) {
            estado = false;
        }
    }

    return estado;
}


public static String descifrado(String textoCifrado, String permutacion) {
    int p = permutacion.length();
    String mensajeDescifrado = "";

    for (int i = 0; i < textoCifrado.length(); i += p) {
        char[] auxGrupo = textoCifrado.substring(i, i + p).toCharArray();
        char[] grupo = new char[p];
        for (int j = 0; j < p; j++) {
            int index = Character.getNumericValue(permutacion.charAt(j) - 1);
            grupo[index] = auxGrupo[j];
        }
        mensajeDescifrado += String.valueOf(grupo);
    }
    return mensajeDescifrado;
}

}