package programacionweb.modelos;
public class Cifrado {

    public static String metodoCifrar(String texto, String permutacion) {
        String mensaje= "";
        if (verificaP(permutacion)) {
            mensaje = cifrado(texto, permutacion);
        }else{
            mensaje ="Error, La permutacion no es valida !";
        }  
        return mensaje;
    }

    public static boolean verificaP(String permutacion) {
        boolean estado = true;

        for (int i = 1; i <= permutacion.length(); i++) {
            if (permutacion.indexOf((char) (i + '0')) == -1) {
                estado = false;
            }
        }

        return estado;
    }

    public static String cifrado(String texto, String permutacion) {
        int p = permutacion.length();
        String mensajeCifrado = "";

        int faltantes = texto.length() % p;
        for (int i = faltantes; i <= p; i++) {
            texto += " ";
        }


        for (int i = 0; i < texto.length() - p; i += p) {
            char[] auxGrupo = texto.substring(i, i + p).toCharArray();
            char[] grupo = new char[p];
            for (int j = 0; j < p; j++) {

                int index = Character.getNumericValue(permutacion.charAt(j) - 1);

                grupo[j] = auxGrupo[index];
            }
            mensajeCifrado += String.valueOf(grupo);
        }
        return mensajeCifrado;
    }

    
}