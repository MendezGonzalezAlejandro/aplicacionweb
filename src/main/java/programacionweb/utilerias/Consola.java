package programacionweb.utilerias;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class Consola {

  private static String marcaDeTiempo() {
    LocalDateTime lDT = LocalDateTime.now();
    DateTimeFormatter oP = DateTimeFormatter.ofPattern("H:m:s:S");
    return lDT.format(oP);
  }

  public static void imprimir(String sujeto, String mensaje) {
    System.out.println("[" + marcaDeTiempo() + "] " + sujeto + " : " + mensaje);
  }

  public static void imprimir(String sujeto, int mensaje) {
    System.out.println("[" + marcaDeTiempo() + "] " + sujeto + " : " + mensaje);
  }

  public static void imprimir(String sujeto, boolean mensaje) {
    System.out.println("[" + marcaDeTiempo() + "] " + sujeto + " : " + mensaje);
  }

}
