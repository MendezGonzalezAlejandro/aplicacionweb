package programacionweb.utilerias;

import javax.servlet.http.HttpServletRequest;

public final class UrlRelativo {

  /*
   * Dado un objeto de la interfaz HttpServlerRequest *solicitud* y una cadena de
   * caracteres *ubicación* regresa un URL relativo hacia la *ubicación*
   *
   * parámetro ubicación = "/recurso/dentro/del/contexto"
   * resultado = "/contexto/recurso/dentro/del/contexto
   *
   * Ejemplo:
   *
   *   parámetro ubicación = "/assets/css/main.css"
   *   resultado = "/contexto/assets/css/main.css"
   */
  public static String para(HttpServletRequest solicitud, String ubicacion) {
    if (solicitud.getServletContext().getContextPath().isEmpty()) {
      return ubicacion;
    }
    return solicitud.getContextPath()  + ubicacion;
  }

  public static String assets(HttpServletRequest solicitud, String ubicacion) {
    return para(solicitud, "/assets" + ubicacion);
  }

  /*
   * Ejemplo:
   *
   *   parámetro ubicación = "/main.css"
   *   resultado = "/contexto/assets/css/main.css"
   */
  public static String css(HttpServletRequest solicitud, String ubicacion) {
    return assets(solicitud, "/css" + ubicacion);
  }

  public static String img(HttpServletRequest solicitud, String ubicacion) {
    return assets(solicitud, "/img" + ubicacion);
  }

  public static String js(HttpServletRequest solicitud, String ubicacion) {
    return assets(solicitud, "/js" + ubicacion);
  }
}
