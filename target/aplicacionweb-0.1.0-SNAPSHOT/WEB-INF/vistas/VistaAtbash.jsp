<%@ page
  	language="java"
  	contentType="text/html;charset=utf-8"
  	pageEncoding="utf-8"
%>

<%@ page
  	import="java.util.List"
  	import="java.util.Map"
  	import="java.util.ArrayList"

    import="programacionweb.utilerias.UrlRelativo"
%>

<%
  String htmlTitle = "VistaInicio.jsp";
  String mainCss = UrlRelativo.css(request, "/main.css");
%>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><%= htmlTitle %></title>
  <link rel="stylesheet" href="<%= mainCss %>">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
  <script src="https://kit.fontawesome.com/7bf7fb481a.js" crossorigin="anonymous"></script>
 
</head>
<body>


  <nav class="navbar" role="navigation" aria-label="dropdown navigation">
    <div class="navbar-item has-dropdown is-hoverable">
      <a class="navbar-link">
        Cifrado Atbash
      </a>
  
      <div class="navbar-dropdown">
        <a class="navbar-item" href="https://mendezgonzalezalejandro.herokuapp.com">
          
          Index 

          <span class="icon">
            <i class="fa fa-book"></i>
          </span>
          
        </a>
        <a class="navbar-item" href="/inicio">
          Transposición por grupo
        </a>

        <a class="navbar-item" href="https://gitlab.com/MendezGonzalezAlejandro/aplicacionweb" >
         
          Commits de la actividad (GitLab) 

          <span class="icon">
            <i class="fab fa-gitlab"></i>
          </span>
          
        </a>
        <hr class="navbar-divider">
        <div class="navbar-item">
          Alejandro Méndez González
        </div>
      </div>
    </div>
  </nav>

  <section class="section content">

    
  <form action="?" method="POST">
    <fieldset>
      
      <div class="control">
        <label for="entrada-de-texto">Ingrese el texto: </label>  
        <input class="input is-focused" type="text" name="entrada-de-texto" placeholder="Ingrese la frase a conmutar....">   
      </div>

      <div id="p">
      <label for="entrada-de-texto">Ingrese el alfabeto: </label>  
      <input class="input is-focused"  type="text" name="entrada-de-alfabeto" placeholder="alfabeto....">
       </div>
 
      <div id="insertar">
        <label for="selector" id="sel">Seleccione</label>
        <div class="select is-rounded">
          <select name="selector">
            <option value="cifrado" >Cifrado</option>
            <option value="descifrado ">Descifrado</option>
          </select>
        </div>
      </div>

      <label for="area-de-texto">Texto cifrado</label>
      <textarea class="textarea is-focused " placeholder="Resultado...." id="bt">${resultado}</textarea>

      <div class="control">
      <textarea class="textarea" placeholder="El alfabeto utilizado fue: ...." disabled>${alfabeto}</textarea>
      </div>
      
  
      <button type="submit" class="button is-success"  name="boton-de-submit" value="botón SUBMIT">Enviar</button>
    
    </fieldset>
  </form>

  </section>

</body>
</html>
